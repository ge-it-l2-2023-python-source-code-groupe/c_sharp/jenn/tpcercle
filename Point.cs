namespace Points;

public class Point
{
    public double X { get; set; }
    public double Y { get; set; }

    public Point(double x, double y)
    {
        X = x;
        Y = y;
    }

    public string Display()
    {
        return $"POINT({X},{Y})";
    }
}

public class Cercle
{
    public Point Centre { get; set; }
    public double Rayon { get; set; }



    public Cercle(Point centre, double rayon)
    {
        Centre = centre;
        Rayon = rayon;

    }
    public double GetPerimeter()
    {
        return 2 * Rayon * 3.14;

    }

    public double GetSurface()
    {
        return Rayon * Rayon * 3.14;
    }


    public void IsInclude(Point p)
    {
        if ((p.X >= Centre.X - Rayon) && (p.X <= Centre.X + Rayon) && (p.Y >= Centre.Y - Rayon) && (p.Y <= Centre.Y + Rayon))
        {
            Console.WriteLine("Le point P appartient au cercle  ");
        }
        else 
        {
            Console.WriteLine("Le point P n'appartient pas au cercle");
        }
    }


    public string Display()
    {
        return $"CERCLE({Centre.X},{Centre.Y},{Rayon})";
    }

}
