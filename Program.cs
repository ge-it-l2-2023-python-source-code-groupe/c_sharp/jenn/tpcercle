using Points;

namespace TP6nov;
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Donner l'abscisse du centre ");
        double x = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("Donner l'ordonné du centre");
        double y = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("Donner le rayon");
        double rayon = Convert.ToDouble(Console.ReadLine());

        Point centre = new Point(x, y);
        Cercle cercle1 = new Cercle(centre, rayon);

        Console.WriteLine(cercle1.Display());
        Console.WriteLine("Le périmètre est" + cercle1.GetPerimeter());
        Console.WriteLine("La surface est " + cercle1.GetSurface());

        Console.WriteLine("Donner un point:");
        double Xpoint = Convert.ToDouble(Console.ReadLine());
        double Ypoint = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("X: "+Xpoint);
        Console.WriteLine("Y: "+Ypoint);
 
        cercle1.IsInclude(centre);
    }
}

